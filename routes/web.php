<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('pages.index');
});

Route::get('store', function () {
    return view('pages.store');
});

Route::get('product', function () {
    return view('pages.product');
});

Route::get('checkout', function () {
    return view('pages.checkout');
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/home', function () {
        return view('admins.pages.index');
    });
});
