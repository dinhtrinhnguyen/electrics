<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product_images extends Model
{
    //
    public function product()
    {
         return $this->belongsTo('products');
    }
}
